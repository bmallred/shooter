package entity

import (
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

func NewPlayer(x, y float32) *shooter.Entity {
	e := shooter.NewEntity()

	e.AddComponent(&component.Spatial{
		Asset:    "player.png",
		Height:   32.0,
		Width:    32.0,
		Rotation: 0.0,
		X:        x,
		Y:        y,
	})

	e.AddComponent(&component.Motion{
		VelocityX: 0.0,
		VelocityY: 0.0,
		Degrade:   0.1,
	})

	e.AddComponent(&component.Health{
		HealthPercentage: 1.0,
		HealthTypes:      []string{"player"},
	})

	e.AddComponent(&component.Damage{
		DamagePercentage: 0.25,
		DamageTypes:      []string{"enemy"},
		DamageSound:      "explosion.wav",
	})

	e.AddComponent(&component.Cannon{
		ProjectileAsset:       "bullet_blue.png",
		ProjectileSound:       "cannon_player.wav",
		ProjectileHeight:      32.0,
		ProjectileWidth:       10.0,
		ProjectileVelocity:    -10.0,
		ProjectileRate:        time.Millisecond * 250,
		ProjectileDamage:      1.0,
		ProjectileDamageTypes: []string{"enemy"},
	})

	e.AddComponent(&component.Control{})

	return e
}

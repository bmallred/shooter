package entity

import (
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

func NewEnemy(x, y float32) *shooter.Entity {
	e := shooter.NewEntity()

	e.AddComponent(&component.Spatial{
		Asset:    "enemy.png",
		Height:   64.0,
		Width:    64.0,
		Rotation: 0.0,
		X:        x,
		Y:        y,
	})

	e.AddComponent(&component.Motion{
		VelocityX: 1.0,
		VelocityY: 0.0,
		Degrade:   0.0,
	})

	e.AddComponent(&component.Health{
		HealthPercentage: 1.0,
		HealthTypes:      []string{"enemy"},
	})

	e.AddComponent(&component.Damage{
		DamagePercentage: 0.75,
		DamageTypes:      []string{"player"},
		DamageSound:      "explosion.wav",
	})

	e.AddComponent(&component.Cannon{
		ProjectileAsset:       "bullet_orange.png",
		ProjectileSound:       "cannon_enemy.wav",
		ProjectileHeight:      32.0,
		ProjectileWidth:       10.0,
		ProjectileVelocity:    10.0,
		ProjectileRate:        time.Millisecond * 450,
		ProjectileDamage:      0.20,
		ProjectileDamageTypes: []string{"player"},
	})

	return e
}

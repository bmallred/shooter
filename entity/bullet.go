package entity

import (
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// NewBullet generates a new bullet entity
func NewBullet(asset, sound string, damage float32, types []string, height, width, rotation, velocity, x, y float32) *shooter.Entity {
	e := shooter.NewEntity()

	e.AddComponent(&component.Spatial{
		Asset:    asset,
		Height:   height,
		Width:    width,
		Rotation: rotation,
		X:        x,
		Y:        y,
	})

	e.AddComponent(&component.Motion{
		VelocityX: 0.0,
		VelocityY: velocity,
		Degrade:   0.0,
	})

	e.AddComponent(&component.Damage{
		DamagePercentage: damage,
		DamageTypes:      types,
		DamageSound:      "explosion.wav",
	})

	e.AddComponent(&component.Sound{
		AudioAsset: sound,
	})

	return e
}

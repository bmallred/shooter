package system

import (
	"math/rand"
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
	"gitlab.com/bmallred/shooter/entity"
)

var (
	enemyFireSeed       = rand.NewSource(time.Now().Unix())
	enemyFireRandomizer = rand.New(enemyFireSeed)
)

// CannonSystem is used to manage the cannon components.
type CannonSystem struct {
	EntityManager shooter.EntityManager
}

// Update the cannon system to fire projectiles as needed.
func (system *CannonSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	// Randomize which enemy ship is firing
	candidates := shooter.FilterComponent(entities, &component.Spatial{}, &component.Cannon{})
	count := len(candidates)
	r := enemyFireRandomizer.Intn(count)

	// Don't fire for the player
	control := candidates[r].GetComponent(&component.Control{})
	if control == nil {
		system.fire(candidates[r])
	} else if count > 1 {
		if r == 0 {
			r += 1
		} else if r == count-1 {
			r -= 1
		}

		if r > -1 {
			system.fire(candidates[r])
		}
	}

	// Fire all rounds
	for _, e := range shooter.FilterComponent(entities, &component.Spatial{}, &component.Cannon{}, &component.Fire{}) {
		// Remove the fire component
		fire := e.GetComponent(&component.Fire{}).(*component.Fire)
		e.RemoveComponent(fire)

		// Fire and forget
		system.fire(e)
	}
}

// fire a projectile from the cannon.
func (system *CannonSystem) fire(e *shooter.Entity) {
	spatial := e.GetComponent(&component.Spatial{}).(*component.Spatial)
	cannon := e.GetComponent(&component.Cannon{}).(*component.Cannon)

	// If the cannon has "cooled down" then allow for more projectiles.
	if time.Since(cannon.LastFired) > cannon.ProjectileRate {
		// Determine the offset on the Y-axis. If the velocity is less than
		// zero then the bullet will move "up" the sceen and the offset needs
		// to be the inverse.
		offset := cannon.ProjectileHeight / 2.0
		if cannon.ProjectileVelocity < 0 {
			offset *= -1
		}

		// Create a new bullet and add it to our entities
		bullet := entity.NewBullet(
			cannon.ProjectileAsset,
			cannon.ProjectileSound,
			cannon.ProjectileDamage,
			cannon.ProjectileDamageTypes,
			cannon.ProjectileHeight,
			cannon.ProjectileWidth,
			270,
			cannon.ProjectileVelocity,
			spatial.X,
			spatial.Y+offset)
		system.EntityManager.Add(bullet)

		// Update the time the cannon was used.
		cannon.LastFired = time.Now()
	}
}

package system

import (
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// GarbageSystem is used to remove entities with trash components.
type GarbageSystem struct {
	EntityManager shooter.EntityManager
	Options       *shooter.GameOptions
}

// Update the will remove entities marked with a trash component.
func (system *GarbageSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	// If there are not any more player controlled entities it is time to stop
	if len(shooter.FilterComponent(entities, &component.Control{})) == 0 {
		system.Options.Stop = true
	}

	// If there are no enemies left then it is time to stop
	if len(shooter.FilterComponent(entities, &component.Cannon{})) <= 1 {
		system.Options.Stop = true
	}

	for _, e := range shooter.FilterComponent(entities, &component.Trash{}) {
		trash := e.GetComponent(&component.Trash{}).(*component.Trash)
		if trash != nil {
			system.EntityManager.Remove(e)
		}
	}
}

package system

import (
	"math"
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// CollisionSystem is used to detect collisions amongst spatial components.
type CollisionSystem struct {
	EntityManager shooter.EntityManager
	Options       *shooter.GameOptions
}

// Update performs any collision detections and the effects.
func (system *CollisionSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	w := float32(system.Options.Width)
	h := float32(system.Options.Height)

	// Check bounds on screens
	for _, e := range shooter.FilterComponent(entities, &component.Spatial{}) {
		spatial := e.GetComponent(&component.Spatial{}).(*component.Spatial)

		halfw := spatial.Width / 2.0
		if spatial.X-halfw < 0 {
			spatial.X = halfw
		}
		if spatial.X+halfw > w {
			spatial.X = w - halfw
		}

		halfh := spatial.Height / 2.0
		if spatial.Y+halfh > h {
			spatial.Y = h - halfh
		}
		if spatial.Y-halfh < 0.0 {
			spatial.Y = halfh
		}
	}

	// Check for collisions which can cause damage
	for _, bullet := range shooter.FilterComponent(entities, &component.Spatial{}, &component.Damage{}) {
		bs := bullet.GetComponent(&component.Spatial{}).(*component.Spatial)
		damage := bullet.GetComponent(&component.Damage{}).(*component.Damage)

		// If the bullet hit the top or bottom of the screen then remove it
		if bs.Y <= bs.Height/2.0 || bs.Y >= h-(bs.Height/2.0) {
			bullet.AddComponent(&component.Trash{})
			continue
		}

		// Inner loop of entities with only spatial representation to be
		// considered.
		for _, e := range shooter.FilterComponent(entities, &component.Spatial{}, &component.Health{}) {
			// Cannot damage self
			if bullet.ID() == e.ID() {
				continue
			}

			// Only damage those targeted
			health := e.GetComponent(&component.Health{}).(*component.Health)
			if !targeted(damage.DamageTypes, health.HealthTypes) {
				continue
			}

			es := e.GetComponent(&component.Spatial{}).(*component.Spatial)
			if collision(bs, es) {
				// Mark the bullet for garbage collection
				bullet.AddComponent(&component.Trash{})

				// Mark the bullet for garbage collection
				e.AddComponent(&component.Sound{
					AudioAsset: damage.DamageSound,
				})

				// Decrease the health of collided entity
				health.HealthPercentage -= damage.DamagePercentage
				break
			}
		}
	}

	// Check for spatial collisions (for enemies) which would require them to ping-pong
	pong := false
	for _, first := range shooter.FilterComponent(entities, &component.Spatial{}, &component.Motion{}, &component.Cannon{}) {
		// If this item is player controllable then skip it
		control := first.GetComponent(&component.Control{})
		if control != nil {
			continue
		}

		spatial := first.GetComponent(&component.Spatial{}).(*component.Spatial)
		halfw := spatial.Width / 2.0
		if spatial.X == halfw || spatial.X == w-halfw {
			pong = true
			break
		}
	}

	if pong {
		// Ping-pong as a hive
		for _, first := range shooter.FilterComponent(entities, &component.Spatial{}, &component.Motion{}, &component.Cannon{}) {
			// If this item is player controllable then skip it
			control := first.GetComponent(&component.Control{})
			if control != nil {
				continue
			}

			spatial := first.GetComponent(&component.Spatial{}).(*component.Spatial)
			motion := first.GetComponent(&component.Motion{}).(*component.Motion)
			motion.VelocityX *= -1
			spatial.Y += spatial.Height / 2.0
		}
	}
}

// collision determines if two spatial entities have detected a collision.
func collision(a, b *component.Spatial) bool {
	distance := euclideanDistance(a.X, a.Y, b.X, b.Y)
	radius := b.Width / 2.0
	return distance <= radius
}

// euclideanDistance calculates the distance between two sets of coordinates on a
// two dimensional plane.
func euclideanDistance(ax, ay, bx, by float32) float32 {
	dx := ax - bx
	dy := ay - by
	ds := (dx * dx) + (dy * dy)
	return float32(math.Sqrt(math.Abs(float64(ds))))
}

// targeted compares damage and health types to determine if damage is
// targeted on a particular entity.
func targeted(damageTypes, healthTypes []string) bool {
	for _, dt := range damageTypes {
		for _, ht := range healthTypes {
			if dt == ht {
				return true
			}
		}
	}

	return false
}

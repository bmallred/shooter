package system

import (
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// HealthSystem is used to manage health metrics.
type HealthSystem struct {
	Options *shooter.GameOptions
}

// Update the health system metrics.
func (system *HealthSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	for _, entity := range shooter.FilterComponent(entities, &component.Health{}) {
		health := entity.GetComponent(&component.Health{}).(*component.Health)
		if health.HealthPercentage <= 0.0 {
			entity.AddComponent(&component.Trash{})
		}
	}
}

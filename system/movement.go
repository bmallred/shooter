package system

import (
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// MovementSystem is used to provide motion to entities.
type MovementSystem struct {
	Options *shooter.GameOptions
}

// Update the location of entities with the motion component.
func (system *MovementSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	shift := float32(delta.Seconds() * float64(system.Options.FramesPerSecond))

	for _, e := range shooter.FilterComponent(entities, &component.Motion{}, &component.Spatial{}) {
		spatial := e.GetComponent(&component.Spatial{}).(*component.Spatial)
		motion := e.GetComponent(&component.Motion{}).(*component.Motion)
		spatial.X += motion.VelocityX + shift
		spatial.Y += motion.VelocityY + shift

		if motion.VelocityX > 0 {
			motion.VelocityX -= motion.Degrade
		}
		if motion.VelocityX < 0 {
			motion.VelocityX += motion.Degrade
		}
		if motion.VelocityY > 0 {
			motion.VelocityY -= motion.Degrade
		}
		if motion.VelocityY < 0 {
			motion.VelocityY += motion.Degrade
		}
	}
}

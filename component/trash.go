package component

// Trash component designates an entity marked for removal.
type Trash struct{}

// Type of component.
func (component *Trash) Type() string {
	return "Trash"
}

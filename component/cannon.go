package component

import "time"

// Cannon component providing an entity the ability to fire
// projectiles.
type Cannon struct {
	ProjectileAsset       string
	ProjectileSound       string
	ProjectileHeight      float32
	ProjectileWidth       float32
	ProjectileVelocity    float32
	ProjectileRate        time.Duration
	ProjectileDamage      float32
	ProjectileDamageTypes []string
	LastFired             time.Time
}

// Type of component.
func (component *Cannon) Type() string {
	return "Cannon"
}

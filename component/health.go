package component

// Health component prodives an entity with health metrics.
type Health struct {
	HealthPercentage float32
	HealthTypes      []string
}

// Type of component.
func (component *Health) Type() string {
	return "Health"
}

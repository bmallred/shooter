package component

// Control component providing user input control.
type Control struct{}

// Type of component.
func (component *Control) Type() string {
	return "Control"
}

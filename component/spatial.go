package component

// Spatial component providing an entity with spatial properties.
type Spatial struct {
	X        float32
	Y        float32
	Asset    string
	Height   float32
	Width    float32
	Rotation float32
}

// Type of component.
func (component *Spatial) Type() string {
	return "Spatial"
}

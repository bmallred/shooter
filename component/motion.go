package component

// Motion component providing an entity movement.
type Motion struct {
	VelocityX float32
	VelocityY float32
	Degrade   float32
}

// Type of component.
func (component *Motion) Type() string {
	return "Motion"
}

package component

// Sound component is used to play an audio blip.
type Sound struct {
	AudioAsset string
}

// Type of component.
func (component *Sound) Type() string {
	return "Sound"
}

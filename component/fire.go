package component

// Fire component is used to designate an entity wants to
// launch a projectile from the cannon.
type Fire struct{}

// Type of component.
func (component *Fire) Type() string {
	return "Fire"
}

package component

// Damage component designates an entity which may incur damage.
type Damage struct {
	DamagePercentage float32
	DamageTypes      []string
	DamageSound      string
}

// Type of component.
func (component *Damage) Type() string {
	return "Damage"
}

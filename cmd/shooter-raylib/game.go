package main

import (
	"sync"
	"time"

	"github.com/gen2brain/raylib-go/raylib"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/assets"
	"gitlab.com/bmallred/shooter/component"
	"gitlab.com/bmallred/shooter/entity"
	"gitlab.com/bmallred/shooter/system"
)

// ShooterGame is the game settings.
type ShooterGame struct {
	removalLock sync.Mutex
	options     *shooter.GameOptions
	sounds      map[string]rl.Sound
	textures    map[string]rl.Texture2D
	entities    []*shooter.Entity
	systems     []shooter.System
}

// Add an entity to the game.
func (game *ShooterGame) Add(entity *shooter.Entity) {
	game.entities = append(game.entities, entity)
}

// Remove an entity to the game.
func (game *ShooterGame) Remove(entity *shooter.Entity) {
	target := -1
	for index, e := range game.entities {
		if e.ID() == entity.ID() {
			target = index
		}
	}

	if target > -1 {
		// With memory leak
		// game.entities = append(game.entities[:target], game.entities[target+1]...)

		// Without memory leak
		copy(game.entities[target:], game.entities[target+1:])
		game.entities[len(game.entities)-1] = nil
		game.entities = game.entities[:len(game.entities)-1]
	}
}

// Preload game assets.
// For raylib this needs to happen after initialization otherwise a panic occurs.
func (game *ShooterGame) Preload() {
	assets.Unpack()

	game.sounds = map[string]rl.Sound{
		"cannon_enemy.wav":  rl.LoadSound("assets/cannon_enemy.wav"),
		"cannon_player.wav": rl.LoadSound("assets/cannon_player.wav"),
		"explosion.wav":     rl.LoadSound("assets/explosion.wav"),
	}

	game.textures = map[string]rl.Texture2D{
		"player.png":        rl.LoadTexture("assets/player.png"),
		"enemy.png":         rl.LoadTexture("assets/enemy.png"),
		"bullet_blue.png":   rl.LoadTexture("assets/bullet_blue.png"),
		"bullet_orange.png": rl.LoadTexture("assets/bullet_orange.png"),
	}
}

// Setup the game environment.
func (game *ShooterGame) Setup() {
	options := game.options

	// Declare entities
	game.Add(entity.NewPlayer(float32(options.Width)/2.0, float32(options.Height)-32))

	// Create some enemies
	rows := 3
	cols := 3
	enemy := entity.NewEnemy(0, 0)
	spatial := enemy.GetComponent(&component.Spatial{}).(*component.Spatial)
	for row := 0; row < rows; row++ {
		y := float32(row+1.0) * spatial.Height

		for col := 0; col < cols; col++ {
			x := (spatial.Width / 2.0) + (float32(col+1) * spatial.Width)
			x += float32(col) * spatial.Width
			if row%2 == 0 {
				x += spatial.Width
			}

			game.Add(entity.NewEnemy(x, y))
		}
	}

	// Declare systems
	game.systems = []shooter.System{
		&ControlSystem{},
		&system.CannonSystem{EntityManager: game},
		&system.MovementSystem{Options: game.options},
		&system.CollisionSystem{EntityManager: game, Options: game.options},
		&system.HealthSystem{Options: game.options},
		&system.GarbageSystem{EntityManager: game, Options: game.options},
		&RenderSystem{game: game},
		&SoundSystem{game: game},
	}
}

// Run the game logic.
func (game *ShooterGame) Run() {
	// Initialize the game context and window properties.
	rl.InitWindow(game.options.Width, game.options.Height, game.options.Title)

	// Setup audio device
	rl.InitAudioDevice()

	// Set the target frames per second.
	rl.SetTargetFPS(game.options.FramesPerSecond)

	// Preload assets once game context has been established.
	game.Preload()

	// Start the timer used for calculating deltas.
	last := time.Now()

	// Game loop should only stop when the window was triggered to close
	// or if the game has been flagged to stop internally.
	for !rl.WindowShouldClose() && !game.options.Stop {
		// Loop through all systems
		for _, system := range game.systems {
			system.Update(time.Since(last), game.entities)
		}

		// Update the timer to calculate the next delta.
		last = time.Now()
	}
}

// Unload game assets and perform any final clean-up routines.
func (game *ShooterGame) Unload() {
	for _, sound := range game.sounds {
		rl.UnloadSound(sound)
	}

	for _, texture := range game.textures {
		rl.UnloadTexture(texture)
	}

	rl.CloseAudioDevice()
	rl.CloseWindow()
}

package main

import (
	"time"

	"github.com/gen2brain/raylib-go/raylib"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// SoundSystem provides the playing of audio files.
type SoundSystem struct {
	game *ShooterGame
}

// Update the game by playing all audio effects.
func (system *SoundSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	for _, entity := range shooter.FilterComponent(entities, &component.Sound{}) {
		sound := entity.GetComponent(&component.Sound{}).(*component.Sound)
		if asset, ok := system.game.sounds[sound.AudioAsset]; ok {
			rl.PlaySound(asset)
		}

		// Remove the sound component so the sound is not looped.
		entity.RemoveComponent(sound)
	}
}

package main

import (
	"time"

	"github.com/gen2brain/raylib-go/raylib"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// ControlSystem provides handling of player inputs.
type ControlSystem struct{}

// Update entities based on player inputs.
func (system *ControlSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	for _, entity := range shooter.FilterComponent(entities, &component.Cannon{}, &component.Spatial{}, &component.Motion{}, &component.Control{}) {
		motion := entity.GetComponent(&component.Motion{}).(*component.Motion)

		if rl.IsKeyDown(rl.KeyS) || rl.IsKeyDown(rl.KeyLeft) {
			motion.VelocityX -= 0.5
		}

		if rl.IsKeyDown(rl.KeyT) || rl.IsKeyDown(rl.KeyRight) {
			motion.VelocityX += 0.5
		}

		// Automatic
		if rl.IsKeyDown(rl.KeySpace) {
			firing := entity.GetComponent(&component.Fire{})
			if firing == nil {
				entity.AddComponent(&component.Fire{})
			}
		}
	}
}

package main

import (
	"time"

	"github.com/gen2brain/raylib-go/raylib"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// RenderSystem provides the bindings to render elements of the game.
type RenderSystem struct {
	game *ShooterGame
}

// Update the game by rendering entities with the spatial component.
func (system *RenderSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	rl.BeginDrawing()
	rl.ClearBackground(rl.DarkGray)

	for _, entity := range shooter.FilterComponent(entities, &component.Spatial{}) {
		c := entity.GetComponent(&component.Spatial{})
		if c == nil {
			panic("Did not find component")
		}

		spatial := c.(*component.Spatial)
		if tx, ok := system.game.textures[spatial.Asset]; ok {
			x := int32(spatial.X - (spatial.Width / 2.0))
			y := int32(spatial.Y - (spatial.Height / 2.0))
			rl.DrawTexture(tx, x, y, rl.RayWhite)
		}
	}

	rl.EndDrawing()
}

package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

func main() {
	filepaths := []string{
		"../../assets/player.png",
		"../../assets/enemy.png",
		"../../assets/bullet_blue.png",
		"../../assets/bullet_orange.png",
		"../../assets/explosion.wav",
		"../../assets/cannon_player.wav",
		"../../assets/cannon_enemy.wav",
	}

	out, err := os.OpenFile("../../assets/assets.go", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		fmt.Printf("Failed to generate file: %s", err)
	}
	defer out.Close()

	header(out)
	for _, fp := range filepaths {
		contents, err := ioutil.ReadFile(fp)
		if err != nil {
			fmt.Printf("Failed to read file: %s", err)
			continue
		}

		_, file := filepath.Split(fp)
		store(out, file, contents)
	}
	footer(out)
}

func header(out io.Writer) {
	fmt.Fprintf(out, `package %s

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
)

func Asset(filename string) ([]byte, error) {
	if contents, ok := bag[filename]; ok {
		return contents, nil
	}
	return ioutil.ReadFile(filename)
}

func Unpack() {
	dir := "assets"
	if _, err := os.Stat(dir); err != nil && os.IsNotExist(err) {
		os.Mkdir(dir, os.ModePerm)
	}

	for file, contents := range bag {
		fp := filepath.Join(dir, file)
		if _, err := os.Stat(fp); err != nil && os.IsNotExist(err) {
			out, err := os.OpenFile(fp, os.O_RDWR|os.O_CREATE, 0644)
			if err != nil {
				log.Printf("Failed to unpack file: %%s", err)
			}
			out.Write(contents)
			out.Close()
		}
	}
}

var bag = map[string][]byte{
`, "assets")
}

func store(out io.Writer, file string, contents []byte) {
	out.Write([]byte{'\t'})
	fmt.Fprintf(out, `"%s": []byte{`, file)

	count := 0
	for n := range contents {
		if count%12 == 0 {
			out.Write([]byte{'\n'})
			out.Write([]byte{'\t', '\t'})
			count = 0
		} else {
			out.Write([]byte{' '})
		}

		fmt.Fprintf(out, "0x%02x,", contents[n])
		count++
	}

	out.Write([]byte{'\n'})
	out.Write([]byte{'\t'})
	fmt.Fprintf(out, `}, `)
	out.Write([]byte{'\n'})
}

func footer(out io.Writer) {
	fmt.Fprintf(out, `}`)
}

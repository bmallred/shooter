package main

import (
	"time"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// ControlSystem provides handling of player inputs.
type ControlSystem struct{}

// Update entities based on player inputs.
func (system *ControlSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	for _, entity := range shooter.FilterComponent(entities, &component.Cannon{}, &component.Spatial{}, &component.Motion{}, &component.Control{}) {
		motion := entity.GetComponent(&component.Motion{}).(*component.Motion)

		if ebiten.IsKeyPressed(ebiten.KeyLeft) || ebiten.IsKeyPressed(ebiten.KeyS) {
			motion.VelocityX -= 0.5
		}

		if ebiten.IsKeyPressed(ebiten.KeyRight) || ebiten.IsKeyPressed(ebiten.KeyT) {
			motion.VelocityX += 0.5
		}

		// Automatic
		if ebiten.IsKeyPressed(ebiten.KeySpace) {
			firing := entity.GetComponent(&component.Fire{})
			if firing == nil {
				entity.AddComponent(&component.Fire{})
			}
		}
	}
}

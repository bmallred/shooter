package main

import (
	"os"

	"gitlab.com/bmallred/shooter"
)

func main() {
	game := &ShooterGame{}
	game.options = &shooter.GameOptions{
		Width:           500,
		Height:          800,
		Title:           "Shooter (ebiten)",
		FramesPerSecond: 60,
		Stop:            false,
	}

	game.Setup()
	game.Run()
	game.Unload()
	os.Exit(0)
}

package main

import (
	"bytes"
	"errors"
	"image"
	_ "image/png"
	"log"
	"sync"
	"time"

	"github.com/hajimehoshi/ebiten"
	"github.com/hajimehoshi/ebiten/audio"
	"github.com/hajimehoshi/ebiten/audio/wav"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/assets"
	"gitlab.com/bmallred/shooter/component"
	"gitlab.com/bmallred/shooter/entity"
	"gitlab.com/bmallred/shooter/system"
)

// ShooterGame is the game settings.
type ShooterGame struct {
	removalLock sync.Mutex
	options     *shooter.GameOptions
	sounds      map[string]*audio.Player
	textures    map[string]*ebiten.Image
	entities    []*shooter.Entity
	systems     []shooter.System
	last        time.Time
	screen      *ebiten.Image
	sound       *audio.Context
}

// Add an entity to the game.
func (game *ShooterGame) Add(entity *shooter.Entity) {
	game.entities = append(game.entities, entity)
}

// Remove an entity to the game.
func (game *ShooterGame) Remove(entity *shooter.Entity) {
	target := -1
	for index, e := range game.entities {
		if e.ID() == entity.ID() {
			target = index
		}
	}

	if target > -1 {
		// Without memory leak
		copy(game.entities[target:], game.entities[target+1:])
		game.entities[len(game.entities)-1] = nil
		game.entities = game.entities[:len(game.entities)-1]
	}
}

// Preload game assets.
func (game *ShooterGame) Preload() {
	game.sounds = map[string]*audio.Player{}
	soundfiles := []string{"cannon_enemy.wav", "cannon_player.wav", "explosion.wav"}
	for _, name := range soundfiles {
		// Pull assets from memory
		wave, err := assets.Asset(name)
		if err != nil {
			log.Println("failed to read sound bytes", err)
			continue
		}

		// Decode the bytes
		d, err := wav.Decode(game.sound, audio.BytesReadSeekCloser(wave))
		if err != nil {
			log.Println("failed to decode sound", err)
			continue
		}

		// Create an audio.Player that has one stream
		player, err := audio.NewPlayer(game.sound, d)
		if err != nil {
			log.Println("failed to create audio player", err)
			continue
		}

		game.sounds[name] = player
	}

	game.textures = map[string]*ebiten.Image{}
	imagefiles := []string{"player.png", "enemy.png", "bullet_blue.png", "bullet_orange.png"}
	for _, name := range imagefiles {
		// Pull assets from memory
		png, err := assets.Asset(name)
		if err != nil {
			log.Println("failed to read image bytes", err)
			continue
		}

		// Decode the bytes
		img, _, err := image.Decode(bytes.NewReader(png))
		if err != nil {
			log.Println("failed to load image", err)
			continue
		}

		texture, err := ebiten.NewImageFromImage(img, ebiten.FilterDefault)
		if err != nil {
			log.Println("failed to load texture", err)
			continue
		}

		game.textures[name] = texture
	}
}

// Setup the game environment.
func (game *ShooterGame) Setup() {
	options := game.options

	// Declare entities
	game.Add(entity.NewPlayer(float32(options.Width)/2.0, float32(options.Height)-32))

	// Create some enemies
	rows := 3
	cols := 3
	enemy := entity.NewEnemy(0, 0)
	spatial := enemy.GetComponent(&component.Spatial{}).(*component.Spatial)
	for row := 0; row < rows; row++ {
		y := float32(row+1.0) * spatial.Height

		for col := 0; col < cols; col++ {
			x := (spatial.Width / 2.0) + (float32(col+1) * spatial.Width)
			x += float32(col) * spatial.Width
			if row%2 == 0 {
				x += spatial.Width
			}

			game.Add(entity.NewEnemy(x, y))
		}
	}

	// Declare systems
	game.systems = []shooter.System{
		&ControlSystem{},
		&system.CannonSystem{EntityManager: game},
		&system.MovementSystem{Options: game.options},
		&system.CollisionSystem{EntityManager: game, Options: game.options},
		&system.HealthSystem{Options: game.options},
		&system.GarbageSystem{EntityManager: game, Options: game.options},
		&RenderSystem{game: game},
		&SoundSystem{game: game},
	}
}

func (game *ShooterGame) update(screen *ebiten.Image) error {
	game.screen = screen

	// Update the timer to calculate the next delta.
	game.last = time.Now()

	// Loop through all systems
	for _, system := range game.systems {
		system.Update(time.Since(game.last), game.entities)
	}

	if game.options.Stop {
		return errors.New("Game over")
	}
	return nil
}

// Run the game logic.
func (game *ShooterGame) Run() {
	// Setup audio device
	ctx, err := audio.NewContext(44100)
	if err != nil {
		log.Fatal(err)
		return
	}
	game.sound = ctx

	// Preload assets once game context has been established.
	game.Preload()

	ebiten.SetRunnableInBackground(true)
	if err := ebiten.Run(game.update, int(game.options.Width), int(game.options.Height), 1, game.options.Title); err != nil {
		log.Println(err)
	}
}

// Unload game assets and perform any final clean-up routines.
func (game *ShooterGame) Unload() {
	for _, sound := range game.sounds {
		sound.Close()
	}

	for _, texture := range game.textures {
		texture.Dispose()
	}
}

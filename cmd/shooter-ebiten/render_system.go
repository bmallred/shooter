package main

import (
	"time"

	"github.com/hajimehoshi/ebiten"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// RenderSystem provides the bindings to render elements of the game.
type RenderSystem struct {
	game *ShooterGame
}

// Update the game by rendering entities with the spatial component.
func (system *RenderSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	screen := system.game.screen
	if ebiten.IsDrawingSkipped() {
		return
	}

	for _, entity := range shooter.FilterComponent(entities, &component.Spatial{}) {
		c := entity.GetComponent(&component.Spatial{})
		if c == nil {
			panic("Did not find component")
		}

		spatial := c.(*component.Spatial)
		if tx, ok := system.game.textures[spatial.Asset]; ok {
			x := float64(spatial.X - (spatial.Width / 2.0))
			y := float64(spatial.Y - (spatial.Height / 2.0))
			options := &ebiten.DrawImageOptions{}
			options.GeoM.Translate(x, y)
			screen.DrawImage(tx, options)
		}
	}
}

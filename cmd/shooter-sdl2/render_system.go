package main

import (
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// RenderSystem provides the bindings to render elements of the game.
type RenderSystem struct {
	game *ShooterGame
}

// Update the game by rendering entities with the spatial component.
func (system *RenderSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	render := system.game.renderer
	render.SetDrawColor(80, 80, 80, 255)
	render.Clear()

	for _, entity := range shooter.FilterComponent(entities, &component.Spatial{}) {
		c := entity.GetComponent(&component.Spatial{})
		if c == nil {
			panic("Did not find component")
		}

		spatial := c.(*component.Spatial)
		if tx, ok := system.game.textures[spatial.Asset]; ok {
			x := int32(spatial.X - (spatial.Width / 2.0))
			y := int32(spatial.Y - (spatial.Height / 2.0))
			src := sdl.Rect{0, 0, int32(spatial.Width), int32(spatial.Height)}
			dst := sdl.Rect{x, y, int32(spatial.Width), int32(spatial.Height)}
			render.Copy(tx, &src, &dst)
		}
	}

	render.Present()
}

package main

import (
	"time"

	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// ControlSystem provides handling of player inputs.
type ControlSystem struct{}

// Update entities based on player inputs.
func (system *ControlSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	for _, entity := range shooter.FilterComponent(entities, &component.Cannon{}, &component.Spatial{}, &component.Motion{}, &component.Control{}) {
		motion := entity.GetComponent(&component.Motion{}).(*component.Motion)

		keys := sdl.GetKeyboardState()

		if keys[sdl.SCANCODE_LEFT] == 1 || keys[sdl.SCANCODE_S] == 1 {
			motion.VelocityX -= 0.5
		}

		if keys[sdl.SCANCODE_RIGHT] == 1 || keys[sdl.SCANCODE_T] == 1 {
			motion.VelocityX += 0.5
		}

		// Automatic
		if keys[sdl.SCANCODE_SPACE] == 1 {
			firing := entity.GetComponent(&component.Fire{})
			if firing == nil {
				entity.AddComponent(&component.Fire{})
			}
		}
	}
}

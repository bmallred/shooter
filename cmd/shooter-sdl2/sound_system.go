package main

import (
	"time"

	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/component"
)

// SoundSystem provides the playing of audio files.
type SoundSystem struct {
	game *ShooterGame
}

// Update the game by playing all audio effects.
func (system *SoundSystem) Update(delta time.Duration, entities []*shooter.Entity) {
	for _, entity := range shooter.FilterComponent(entities, &component.Sound{}) {
		sound := entity.GetComponent(&component.Sound{}).(*component.Sound)
		if asset, ok := system.game.sounds[sound.AudioAsset]; ok {
			asset.Play(-1, 0)
		}

		// Remove the sound component so the sound is not looped.
		entity.RemoveComponent(sound)
	}
}

package main

import (
	"log"
	"sync"
	"time"

	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/mix"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/bmallred/shooter"
	"gitlab.com/bmallred/shooter/assets"
	"gitlab.com/bmallred/shooter/component"
	"gitlab.com/bmallred/shooter/entity"
	"gitlab.com/bmallred/shooter/system"
)

// ShooterGame is the game settings.
type ShooterGame struct {
	removalLock sync.Mutex
	options     *shooter.GameOptions
	sounds      map[string]*mix.Chunk
	textures    map[string]*sdl.Texture
	entities    []*shooter.Entity
	systems     []shooter.System
	renderer    *sdl.Renderer
}

// Add an entity to the game.
func (game *ShooterGame) Add(entity *shooter.Entity) {
	game.entities = append(game.entities, entity)
}

// Remove an entity to the game.
func (game *ShooterGame) Remove(entity *shooter.Entity) {
	target := -1
	for index, e := range game.entities {
		if e.ID() == entity.ID() {
			target = index
		}
	}

	if target > -1 {
		// Without memory leak
		copy(game.entities[target:], game.entities[target+1:])
		game.entities[len(game.entities)-1] = nil
		game.entities = game.entities[:len(game.entities)-1]
	}
}

// Preload game assets.
func (game *ShooterGame) Preload() {
	assets.Unpack()

	game.sounds = map[string]*mix.Chunk{}
	soundfiles := []string{"cannon_enemy.wav", "cannon_player.wav", "explosion.wav"}
	for _, name := range soundfiles {
		wave, err := assets.Asset(name)
		if err != nil {
			log.Println("failed to read sound bytes", err)
			continue
		}

		chunk, err := mix.QuickLoadWAV(wave)
		if err != nil {
			log.Println("failed to load wave", err)
			continue
		}
		game.sounds[name] = chunk
	}

	game.textures = map[string]*sdl.Texture{}
	imagefiles := []string{"player.png", "enemy.png", "bullet_blue.png", "bullet_orange.png"}
	for _, name := range imagefiles {
		image, err := img.Load("assets/" + name)
		if err != nil {
			log.Println("failed to load image", err)
			continue
		}
		defer image.Free()

		texture, err := game.renderer.CreateTextureFromSurface(image)
		if err != nil {
			log.Println("failed to load texture", err)
			continue
		}
		game.textures[name] = texture
	}
}

// Setup the game environment.
func (game *ShooterGame) Setup() {
	options := game.options

	// Declare entities
	game.Add(entity.NewPlayer(float32(options.Width)/2.0, float32(options.Height)-32))

	// Create some enemies
	rows := 3
	cols := 3
	enemy := entity.NewEnemy(0, 0)
	spatial := enemy.GetComponent(&component.Spatial{}).(*component.Spatial)
	for row := 0; row < rows; row++ {
		y := float32(row+1.0) * spatial.Height

		for col := 0; col < cols; col++ {
			x := (spatial.Width / 2.0) + (float32(col+1) * spatial.Width)
			x += float32(col) * spatial.Width
			if row%2 == 0 {
				x += spatial.Width
			}

			game.Add(entity.NewEnemy(x, y))
		}
	}

	// Declare systems
	game.systems = []shooter.System{
		&ControlSystem{},
		&system.CannonSystem{EntityManager: game},
		&system.MovementSystem{Options: game.options},
		&system.CollisionSystem{EntityManager: game, Options: game.options},
		&system.HealthSystem{Options: game.options},
		&system.GarbageSystem{EntityManager: game, Options: game.options},
		&RenderSystem{game: game},
		&SoundSystem{game: game},
	}
}

// Run the game logic.
func (game *ShooterGame) Run() {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		log.Println("failed to load everything", err)
		return
	}

	window, err := sdl.CreateWindow(game.options.Title, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, game.options.Width, game.options.Height, sdl.WINDOW_SHOWN)
	if err != nil {
		log.Println("failed to create window", err)
		return
	}
	defer window.Destroy()

	// Setup audio device
	if err := mix.OpenAudio(44100, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
		log.Println("failed to open audio", err)
		return
	}
	defer mix.CloseAudio()

	// Create a renderer. This will be used for preloading assets as well as
	// for the rendering system.
	renderer, err := sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		log.Println("Failed to create renderer")
		return
	}
	game.renderer = renderer

	// Preload assets once game context has been established.
	game.Preload()

	for !game.options.Stop {
		// Update the timer to calculate the next delta.
		last := time.Now()

		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				game.options.Stop = true
				break
			}
		}

		// Loop through all systems
		for _, system := range game.systems {
			system.Update(time.Since(last), game.entities)
		}

		// Adjust for target FPS
		sdl.Delay(uint32(1000 / game.options.FramesPerSecond))
	}
}

// Unload game assets and perform any final clean-up routines.
func (game *ShooterGame) Unload() {
	for _, sound := range game.sounds {
		sound.Free()
	}

	for _, texture := range game.textures {
		texture.Destroy()
	}

	if game.renderer != nil {
		game.renderer.Destroy()
	}

	sdl.Quit()
}

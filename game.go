package shooter

// Game interface describes the basic cycles of a game.
type Game interface {
	// Preload game assets.
	Preload()

	// Setup the game environment.
	Setup()

	// Run the game logic.
	Run()

	// Unload game assets and perform any final clean-up routines.
	Unload()
}

// GameOptions represents common information about the game to be
// used by core systems and components.
type GameOptions struct {
	Width           int32
	Height          int32
	Title           string
	FramesPerSecond int32
	Stop            bool
}

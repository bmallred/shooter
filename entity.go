package shooter

import (
	"sync"
	"sync/atomic"
)

var (
	counterLock sync.Mutex
	idInc       uint64
)

// Identifier interface to retrieve the entity's unique identifier.
type Identifier interface {
	ID() uint64
}

// EntityManager interface is used to manage storage of entities within
// the world and systems.
type EntityManager interface {
	Add(entity *Entity)
	Remove(entity *Entity)
}

// Entity is a light structure composed of components.
type Entity struct {
	id         uint64
	components []Component
}

// NewEntity creates a basic entity with a unique identifier.
func NewEntity() *Entity {
	return &Entity{id: atomic.AddUint64(&idInc, 1)}
}

// ID returns the entity unique identifier.
func (entity *Entity) ID() uint64 {
	return entity.id
}

// Components returns all associated components.
func (entity *Entity) Components() []Component {
	return entity.components
}

// AddComponent adds one or more components to the entity.
func (entity *Entity) AddComponent(components ...Component) {
	// TODO: Add checks to make sure a component has not already been added.
	for _, component := range components {
		entity.components = append(entity.components, component)
	}
}

// RemoveComopont removes one or more components from the entity.
func (entity *Entity) RemoveComponent(components ...Component) {
	marked := []int{}
	for i, assigned := range entity.components {
		for _, component := range components {
			if assigned.Type() == component.Type() {
				marked = append(marked, i)
				break
			}
		}
	}

	for _, mark := range marked {
		entity.components = append(entity.components[:mark], entity.components[mark+1:]...)
	}
}

// GetComponent attempts to retrieve the requested component by the type. If none
// were found `nil` is returned.
func (entity *Entity) GetComponent(component Component) Component {
	for _, assigned := range entity.components {
		if assigned.Type() == component.Type() {
			return assigned
		}
	}

	return nil
}

// FilterComponent returns an array of entities which have all of the requested components.
func FilterComponent(entities []*Entity, components ...Component) []*Entity {
	matching := []*Entity{}

	for _, entity := range entities {
		matches := 0
		for _, component := range components {
			if entity.GetComponent(component) != nil {
				matches = matches + 1
			}
		}

		if matches >= len(components) {
			matching = append(matching, entity)
		}

	}

	return matching
}

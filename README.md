# shooter

Just a quick test on game frameworks and libraries using the Go language.

## Credits

### raylib

 - **License**: zlib/libpng
 - **Source**: Ramon Santamaria (raysan5)
 - **Link**: https://github.com/raysan5/raylib

### raylib-go

 - **License**: zlib
 - **Source**: Milan Nikolic (gen2brain)
 - **Link**: https://github.com/gen2brain/raylib-go

### enemy.png, player.png, bullet_blue.png, bullet_orange.png

 - **License**: Unknown
 - **Source**: _Out There Somewhere_ (C) 2016 by MiniBoss
 - **Link**: https://www.spriters-resource.com/download/79665/

### explosion.wav

 - **License**: CC0
 - **Source**: thehorriblejoke
 - **Link**: https://freesound.org/people/thehorriblejoke/sounds/259962/

### cannon_player.wav

 - **License**: CC BY 3.0
 - **Source**: MusicLegends
 - **Link**: https://freesound.org/people/MusicLegends/sounds/344310/

### cannon_enemy.wav

 - **License**: CC BY 3.0
 - **Source**: LittleRobotSoundFactory
 - **Link**: https://freesound.org/people/LittleRobotSoundFactory/sounds/270343/

